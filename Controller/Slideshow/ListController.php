<?php

namespace Nitra\SlideshowBundle\Controller\Slideshow;

use Admingenerated\NitraSlideshowBundle\BaseSlideshowController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function getQuery()
    {
        $session = $this->getRequest()->getSession();

        $query = $this->buildQuery();
        //выводим слайды текущего магазина и слайды не относящиеся ни к одному из магазинов
        $query->addOr($query->expr()->field('stores.$id')->equals(new \MongoId($session->get('store_id'))));
        $query->addOr($query->expr()->field('stores')->exists(false));

        $this->processSort($query);
        $this->processFilters($query);
        $this->processScopes($query);

        return $query;
    }
}