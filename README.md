# SlideshowBundle

## Описание

данный бандл предназначен для:

* **создания и редактирования слайдов**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-slideshowbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\SlideshowBundle\NitraSlideshowBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```